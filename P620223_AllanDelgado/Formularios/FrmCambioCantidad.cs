﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P620223_AllanDelgado.Formularios
{
    public partial class FrmCambioCantidad : Form
    {

        /*
        ================================================================================
                                       Examen Final
                                      ROBERT CHAVES P
        ================================================================================
        */

        public int IndexDtListaItems { get; set; }

        #region PropiedadesDeTotalizacion
        public decimal SubTotal1 { get; set; }
        public decimal TotalDescuento { get; set; }
        public decimal SubTotal2 { get; set; }
        public decimal TotalImpuesto { get; set; }
        public decimal Total { get; set; }
        public decimal PrecioUnitario { set; get; }
        public decimal TasaImpuesto { get; set; }
        public decimal PorcentajeDescuento { get; set; }
        public decimal Cantidad { get; set; }
        #endregion


        public FrmCambioCantidad()
        {
            InitializeComponent();
            IndexDtListaItems = 0;
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            
            if (Convert.ToInt32(NumericUpDown.Value) > 0)
            {

                IndexDtListaItems = Globales.IndexDataTableGlobal;

                ProcesarParametros();

                this.DialogResult = DialogResult.OK;

            }
            else
            {
                MessageBox.Show("No se permiten valores menores a 1", ":(", MessageBoxButtons.OK);
            }
        }

        private void FrmCambioCantidad_Load(object sender, EventArgs e)
        {
            NumericUpDown.Value = Globales.MiFacturaInventarioGlobal.Cantidad;
        }

        private void ProcesarParametros()
        {
            //Este método calcula los valores de
            //subtotal, descuento, impuesto y total para la línea 
            //en este caso particular se debe validar que el descuento
            //no supere el 100%

           
            PrecioUnitario = Convert.ToDecimal(Globales.MiFormFacturacion.DtListaItems.Rows[this.IndexDtListaItems][2]);
            Globales.MiFormFacturacion.DtListaItems.Rows[IndexDtListaItems][3] = Convert.ToInt32(NumericUpDown.Value);
            Cantidad = Convert.ToInt32(NumericUpDown.Value);
            TasaImpuesto = Convert.ToDecimal(Globales.MiFormFacturacion.DtListaItems.Rows[this.IndexDtListaItems][4]); //TasaImpuesto
            PorcentajeDescuento = Convert.ToDecimal(Globales.MiFormFacturacion.DtListaItems.Rows[this.IndexDtListaItems][5]); //PorcentajeDescuento 
            SubTotal1 = Convert.ToDecimal(Globales.MiFormFacturacion.DtListaItems.Rows[this.IndexDtListaItems][6]); //SubTotal1
            SubTotal2 = Convert.ToDecimal(Globales.MiFormFacturacion.DtListaItems.Rows[this.IndexDtListaItems][7]); //SubTotal2
            //TotalImpuesto = Convert.ToDecimal(Globales.MiFormFacturacion.DtListaItems.Rows[this.IndexDtListaItems][8]); //TotalImpuesto 
            TotalImpuesto = Convert.ToDecimal(Globales.MiFormFacturacion.DtListaItems.Rows[this.IndexDtListaItems][9]); //Total
            Total = Convert.ToDecimal(Globales.MiFormFacturacion.DtListaItems.Rows[this.IndexDtListaItems][10]); //TotalDescuento

            Calcular();

            //revisados
            Globales.MiFormFacturacion.DtListaItems.Rows[this.IndexDtListaItems][7] = TotalDescuento;
            //Globales.MiFormFacturacion.DtListaItems.Rows[this.IndexDtListaItems][8] = 999; //TotalImpuesto 
            Globales.MiFormFacturacion.DtListaItems.Rows[this.IndexDtListaItems][6] = SubTotal1; 
            Globales.MiFormFacturacion.DtListaItems.Rows[this.IndexDtListaItems][9] = TotalImpuesto; 
            Globales.MiFormFacturacion.DtListaItems.Rows[this.IndexDtListaItems][10] = Total;
        }


        private void Calcular()
        {
            SubTotal1 = Cantidad * PrecioUnitario;
            if (PorcentajeDescuento > 0)
            {
                TotalDescuento = (SubTotal1 * PorcentajeDescuento) / 100;
            }
            SubTotal2 = SubTotal1 - TotalDescuento;
            if (TasaImpuesto > 0)
            {
                TotalImpuesto = (SubTotal2 * TasaImpuesto) / 100;
            }
            Total = SubTotal2 + TotalImpuesto;
        }

       

    }
}
